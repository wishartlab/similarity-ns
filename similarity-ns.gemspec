Gem::Specification.new do |s|
  s.name = "similarity-NS"
  s.version = "0.0.1"
  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.description = <<-EOT
Document similarity calculations using cosine similarity and TF-IDF weights
EOT
  s.authors       = ["Zachary Budinski"]
  s.files = Dir["lib/**/*"]
  s.require_paths = ["lib"]
  s.summary = %q{Document similarity calculations using cosine similarity and TF-IDF weights. Namespaced By Zachary Budinski.}
  s.add_dependency "rb-gsl", ">=1.16.0"
  s.add_development_dependency "rake"
  s.add_development_dependency "faker"
  s.add_development_dependency "ruby-graphviz"
end
